<?php

$lang['date_app_description'] = 'Інструмент для синхронізації часового поясу та годинника.';
$lang['date_app_name'] = 'Дата і час';
$lang['date_app_tooltip'] = 'VoIP служби аутентифікації та інші програми вимагають правильного налаштування часу, тому переконайтеся, що ваш годинник і часовий пояс правильні!';
$lang['date_automatic_synchronize'] = 'Автоматична синхронізація';
$lang['date_date'] = 'Дата';
$lang['date_synchronize'] = 'Синхронізувати';
$lang['date_synchronize_now'] = 'Синхронізувати зараз';
$lang['date_synchronize_wizard_tip'] = 'Якщо увімкнено автоматичну синхронізацію, система буде час від часу синхронізувати свій годинник.';
$lang['date_synchronized'] = 'Синхронізовано';
$lang['date_synchronizing'] = 'Синхронізація';
$lang['date_time'] = 'Час';
$lang['date_time_server'] = 'Сервер часу';
$lang['date_time_server_is_invalid'] = 'Сервер часу недійсний.';
$lang['date_time_synchronization_schedule_is_invalid'] = 'Недійсний графік синхронізації часу.';
$lang['date_time_zone'] = 'Часовий пояс';
$lang['date_time_zone_is_invalid'] = 'Часовий пояс недійсний.';
$lang['date_time_zone_wizard_help'] = 'Для багатьох програм і служб потрібна правильна інформація про часовий пояс і точний годинник, тому важливо, щоб вони були налаштовані правильно.';
